import java.util.ArrayList;

public class Citizenship extends Exception {
    ArrayList<String> allowed = new ArrayList<>();
    public boolean found = false;

    public void checkEnter(String citizenship) throws CitizenshipException {
        for (int i = 0; i < allowed.size(); i++) {
            if (allowed.get(i) == citizenship) {
                found = true;
                break;
            }
        }
        if (found == true) {
            System.out.println("you are allowed to enter");
        } else {
            throw new CitizenshipException("e");
        }
    }
}